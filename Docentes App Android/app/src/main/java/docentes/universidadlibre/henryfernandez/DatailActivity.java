package docentes.universidadlibre.henryfernandez;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import docentes.universidadlibre.henryfernandez.Models.Schedules;

public class DatailActivity extends AppCompatActivity {

    private TextView subject, time, day_week, classroom, description;
    private TextInputEditText message;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private RelativeLayout btnEnviar;
    private RequestQueue queue;
    private Schedules schedules;
    private LinearLayout parent;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datail);
        queue = Volley.newRequestQueue(this);
        initToolbar();
        initComponents();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Docente");
    }

    private void initComponents() {
        subject = (TextView) findViewById(R.id.txtSubjects);
        message = (TextInputEditText) findViewById(R.id.message);
        time = (TextView) findViewById(R.id.time);
        day_week = (TextView) findViewById(R.id.day_week);
        classroom = (TextView) findViewById(R.id.classroom);
        description = (TextView) findViewById(R.id.description);
        btnEnviar = (RelativeLayout) findViewById(R.id.btnEnviar);
        parent = (LinearLayout) findViewById(R.id.parent);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        schedules = getIntent().getParcelableExtra("schedules");
        if (schedules != null) {
            subject.setText(schedules.getSubject());
            time.setText(schedules.getTime_start() + " / " + schedules.getTime_end());
            day_week.setText(schedules.getDay_week());
            classroom.setText(schedules.getClassroom());
            description.setText(schedules.getDescription());
        }

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                parent.setVisibility(View.GONE);
                sendData();
            }
        });
    }

    private void sendData() {
        String url = getString(R.string.base_url) + getString(R.string.daily_record_url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Toast.makeText(DatailActivity.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            Log.e("DOCENTES", "onResponse: " + jsonObject.getString("message").toString());
                            if (jsonObject.getBoolean("flag")) {
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("DOCENTES", "onResponse: " + e.toString());
                            Toast.makeText(DatailActivity.this, e.toString(), Toast.LENGTH_LONG).show();
                        }
                        progressBar.setVisibility(View.GONE);
                        parent.setVisibility(View.VISIBLE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(DatailActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                parent.setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("topic", message.getText().toString().trim().toLowerCase());
                params.put("schedule_id", String.valueOf(schedules.getIdTable()));
                return params;
            }
        };
        queue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
