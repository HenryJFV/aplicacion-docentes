package docentes.universidadlibre.henryfernandez.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "schedules")
public class Schedules implements Parcelable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(index = true, canBeNull = false)
    private int idTable;

    @DatabaseField
    public String day_week;

    @DatabaseField
    public String time_start;

    @DatabaseField
    public String time_end;

    @DatabaseField
    public String classroom;

    @DatabaseField
    public int userId;

    @DatabaseField
    public int subjectId;

    @DatabaseField
    public String subject;

    @DatabaseField
    public String description;


    public Schedules() {
    }

    public Schedules(int idTable, String day_week, String time_start, String time_end, String classroom,
                     int userId, int subjectId, String subject, String description) {
        this.idTable = idTable;
        this.day_week = day_week;
        this.time_start = time_start;
        this.time_end = time_end;
        this.classroom = classroom;
        this.userId = userId;
        this.subjectId = subjectId;
        this.subject = subject;
        this.description = description;
    }

    public int getIdTable() {
        return idTable;
    }

    public void setIdTable(int idTable) {
        this.idTable = idTable;
    }

    public String getDay_week() {
        return day_week;
    }

    public void setDay_week(String day_week) {
        this.day_week = day_week;
    }

    public String getTime_start() {
        return time_start;
    }

    public void setTime_start(String time_start) {
        this.time_start = time_start;
    }

    public String getTime_end() {
        return time_end;
    }

    public void setTime_end(String time_end) {
        this.time_end = time_end;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Schedules{" +
                "id=" + id +
                ", idTable=" + idTable +
                ", day_week='" + day_week + '\'' +
                ", time_start='" + time_start + '\'' +
                ", time_end='" + time_end + '\'' +
                ", classroom='" + classroom + '\'' +
                ", userId=" + userId +
                ", subjectId=" + subjectId +
                ", subject='" + subject + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.idTable);
        dest.writeString(this.day_week);
        dest.writeString(this.time_start);
        dest.writeString(this.time_end);
        dest.writeString(this.classroom);
        dest.writeInt(this.userId);
        dest.writeInt(this.subjectId);
        dest.writeString(this.subject);
        dest.writeString(this.description);
    }

    protected Schedules(Parcel in) {
        this.id = in.readInt();
        this.idTable = in.readInt();
        this.day_week = in.readString();
        this.time_start = in.readString();
        this.time_end = in.readString();
        this.classroom = in.readString();
        this.userId = in.readInt();
        this.subjectId = in.readInt();
        this.subject = in.readString();
        this.description = in.readString();
    }

    public static final Parcelable.Creator<Schedules> CREATOR = new Parcelable.Creator<Schedules>() {
        @Override
        public Schedules createFromParcel(Parcel source) {
            return new Schedules(source);
        }

        @Override
        public Schedules[] newArray(int size) {
            return new Schedules[size];
        }
    };
}
