package docentes.universidadlibre.henryfernandez;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import docentes.universidadlibre.henryfernandez.Adapters.Adapter;
import docentes.universidadlibre.henryfernandez.Adapters.AdapterRecords;
import docentes.universidadlibre.henryfernandez.DataBase.DatabaseHelper;
import docentes.universidadlibre.henryfernandez.DataBase.UserDao;
import docentes.universidadlibre.henryfernandez.Models.Daily_record;
import docentes.universidadlibre.henryfernandez.Models.Schedules;
import docentes.universidadlibre.henryfernandez.Models.User;

public class Daily_recordsActivity extends AppCompatActivity {

    private RequestQueue queue;
    private ActionBar actionBar;
    private Toolbar toolbar;

    private RecyclerView recyclerView;
    private AdapterRecords mAdapter;

    List<Daily_record> list;
    User user;
    private SwipeRefreshLayout swipe_refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_records);
        queue = Volley.newRequestQueue(this);
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });
        initToolbar();
        getData(false);
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Planilla");

        user = new UserDao().getUser(this);
        if (user != null) {
            //actionBar.setTitle(user.getFullName());
        } else {

            new DatabaseHelper(this).resetTables();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            actionBar.setTitle("OBJETO NULO");
        }

    }

    private void initComponent() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new AdapterRecords(this, list);
        recyclerView.setAdapter(mAdapter);

/*        // on item list clicked
        mAdapter.setOnItemClickListener(new Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Schedules obj, int position) {
                startActivity(new Intent(getApplicationContext(), DatailActivity.class).putExtra("schedules", obj));
                Snackbar.make(parent_view, "Item " + obj.subject + " clicked", Snackbar.LENGTH_SHORT).show();
            }
        });*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void getData(final boolean flag) {
        list = new ArrayList<>(0);
        String url = getString(R.string.base_url) + getString(R.string.get_daily_record_url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("flag")) {
                        JSONArray array = jsonObject.getJSONArray("response");
                        new DatabaseHelper(getApplicationContext()).resetDaily_records();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            list.add(new Daily_record(object.getString("created_at"), object.getString("name"),
                                    object.getString("topic")));
                        }
                        initComponent();
                        if (flag) swipe_refresh.setRefreshing(false);

                    } else {
                        Toast.makeText(Daily_recordsActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        if (flag) swipe_refresh.setRefreshing(false);
                    }

                } catch (Exception e) {
                    Toast.makeText(Daily_recordsActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    if (flag) swipe_refresh.setRefreshing(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Daily_recordsActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                if (flag) swipe_refresh.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", String.valueOf(user.getUserId()));
                return params;
            }
        };
        queue.add(stringRequest);
    }
}
