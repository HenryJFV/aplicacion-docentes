package docentes.universidadlibre.henryfernandez.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import docentes.universidadlibre.henryfernandez.Models.Daily_record;
import docentes.universidadlibre.henryfernandez.Models.Schedules;
import docentes.universidadlibre.henryfernandez.Models.User;
import docentes.universidadlibre.henryfernandez.R;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "docente.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<User, Integer> userDao = null;
    private RuntimeExceptionDao<User, Integer> userRuntimeDao = null;

    private Dao<Schedules, Integer> schedulesDao = null;
    private RuntimeExceptionDao<Schedules, Integer> schedulesRuntimeDao = null;

    private Dao<Daily_record, Integer> daily_recordDao = null;
    private RuntimeExceptionDao<Daily_record, Integer> daily_recordRuntimeDao = null;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            Log.i(DatabaseHelper.class.getSimpleName(), "onCreate()");
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Schedules.class);
            TableUtils.createTable(connectionSource, Daily_record.class);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getSimpleName(), e.toString());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource,User.class, true);
            TableUtils.dropTable(connectionSource,Schedules.class, true);
            TableUtils.dropTable(connectionSource,Daily_record.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
            Log.e(DatabaseHelper.class.getSimpleName(), e.toString());
            throw new RuntimeException(e);
        }
    }

    public Dao<User, Integer> getUserDao() throws SQLException{
        if (userDao == null) userDao = getDao(User.class);
        return userDao;
    }

    public void setUserDao(Dao<User, Integer> userDao) {
        this.userDao = userDao;
    }

    public RuntimeExceptionDao<User, Integer> getUserRuntimeDao() throws SQLException {
        if (userRuntimeDao == null) userRuntimeDao = getRuntimeExceptionDao(User.class);
        return userRuntimeDao;
    }

    public void setUserRuntimeDao(RuntimeExceptionDao<User, Integer> userRuntimeDao) {
        this.userRuntimeDao = userRuntimeDao;
    }

    public Dao<Schedules, Integer> getSchedulesDao() {
        return schedulesDao;
    }

    public void setSchedulesDao(Dao<Schedules, Integer> schedulesDao) {
        this.schedulesDao = schedulesDao;
    }

    public RuntimeExceptionDao<Schedules, Integer> getSchedulesRuntimeDao() {
        return schedulesRuntimeDao;
    }

    public void setSchedulesRuntimeDao(RuntimeExceptionDao<Schedules, Integer> schedulesRuntimeDao) {
        this.schedulesRuntimeDao = schedulesRuntimeDao;
    }

    public Dao<Daily_record, Integer> getDaily_recordDao() {
        return daily_recordDao;
    }

    public void setDaily_recordDao(Dao<Daily_record, Integer> daily_recordDao) {
        this.daily_recordDao = daily_recordDao;
    }

    public RuntimeExceptionDao<Daily_record, Integer> getDaily_recordRuntimeDao() {
        return daily_recordRuntimeDao;
    }

    public void setDaily_recordRuntimeDao(RuntimeExceptionDao<Daily_record, Integer> daily_recordRuntimeDao) {
        this.daily_recordRuntimeDao = daily_recordRuntimeDao;
    }

    public void resetUser() {
        try {
            ConnectionSource source = this.getConnectionSource();
            TableUtils.dropTable(source, User.class, true);
            TableUtils.createTable(source, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void resetSchedules() {
        try {
            ConnectionSource source = this.getConnectionSource();
            TableUtils.dropTable(source, Schedules.class, true);
            TableUtils.createTable(source, Schedules.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void resetDaily_records() {
        try {
            ConnectionSource source = this.getConnectionSource();
            TableUtils.dropTable(source, Daily_record.class, true);
            TableUtils.createTable(source, Daily_record.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void resetTables(){
        resetUser();
        resetSchedules();
        resetDaily_records();
    }

    @Override
    public void close() {
        super.close();
        userDao = null;
        userRuntimeDao = null;
        schedulesDao = null;
        schedulesRuntimeDao = null;
        daily_recordDao = null;
        daily_recordRuntimeDao = null;
    }
}
