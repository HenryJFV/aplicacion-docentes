package docentes.universidadlibre.henryfernandez.DataBase;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.util.List;

import docentes.universidadlibre.henryfernandez.Models.User;

public class UserDao {

    private DatabaseHelper helper;

    public  boolean crate(User user, Context context){
        boolean res = true;
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            RuntimeExceptionDao<User, Integer> UserDao = helper.getUserRuntimeDao();
            UserDao.create(user); //delete, update
        }catch (Exception e){
            res = false;
            Log.e("UserController", "error: ", e);
        }

        return res;
    }
    public boolean delete(User user, Context context){
        boolean res = true;
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            RuntimeExceptionDao<User, Integer> UserDao = helper.getUserRuntimeDao();
            UserDao.delete(user); //delete, update
        }catch (Exception e){
            res = false;
            Log.e("UserController", "error: ", e);
        }

        return res;
    }
    public boolean update(User user, Context context){
        boolean res = true;
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            RuntimeExceptionDao<User, Integer> UserDao = helper.getUserRuntimeDao();
            UserDao.update(user); //delete, update
        }catch (Exception e){
            res = false;
            Log.e("UserController", "error: ", e);
        }

        return res;
    }
    public List<User> getUsers(Context context){
        List<User> listaUsers = null;
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            RuntimeExceptionDao<User, Integer> UserDao = helper.getUserRuntimeDao();
            listaUsers = UserDao.queryForAll();
        }catch (Exception e){
        }
        return listaUsers;
    }
    public User getUser(Context context){
        User user = null;
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            RuntimeExceptionDao<User, Integer> UserDao = helper.getUserRuntimeDao();
            user = UserDao.queryForAll().get(0);
        }catch (Exception e){
            user = null;
        }
        return user;
    }
    public User getUserById(String userId, Context context){
        User user = null;
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            RuntimeExceptionDao<User, Integer> UserDao = helper.getUserRuntimeDao();
            user = UserDao.queryForEq("id",userId).get(0);
        }catch (Exception e){
            user = null;
        }
        return user;
    }
}


