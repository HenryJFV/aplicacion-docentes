package docentes.universidadlibre.henryfernandez;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.j256.ormlite.stmt.query.In;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import docentes.universidadlibre.henryfernandez.Adapters.*;
import docentes.universidadlibre.henryfernandez.DataBase.DatabaseHelper;
import docentes.universidadlibre.henryfernandez.DataBase.UserDao;
import docentes.universidadlibre.henryfernandez.Models.Schedules;
import docentes.universidadlibre.henryfernandez.Models.User;

public class MainActivity extends AppCompatActivity {

    Calendar calendar;
    SimpleDateFormat format;

    private RequestQueue queue;
    private ActionBar actionBar;
    private Toolbar toolbar;

    private RecyclerView recyclerView;
    private Adapter mAdapter;

    private LinearLayout parent_view;

    List<Schedules> list;
    User user;

    private SwipeRefreshLayout swipe_refresh;

    LocationManager locationManager;
    LocationListener locationListener;

    TextView locationText;

    private int MILISEGUNDOS_TIEMPO_GPS = 1000 * 10;
    Context mContext;

    static Socket socket;
    int counter = 0;

    {
        try {
            //socket = IO.socket("https://services-docentes.herokuapp.com/");
            socket = IO.socket("http://192.168.1.103:4000/");
        } catch (URISyntaxException e) {
            Log.e("SOCKET.IO", "instance initializer: " + e.toString());
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        calendar = Calendar.getInstance();
        format = new SimpleDateFormat("HH:mm");
        queue = Volley.newRequestQueue(this);
        parent_view = (LinearLayout) findViewById(R.id.parent);
        locationText = (TextView) findViewById(R.id.locationTxt);
        initToolbar();
        initNavigationMenu();
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });
        getData(false);

        socket.connect();

        locationSetup();


    }

    public void verificar(String json) throws ParseException {
        Calendar now = Calendar.getInstance();
        String[] strDays = new String[]{
                "Domingo",
                "Lunes",
                "Martes",
                "Miercoles",
                "Jueves",
                "Viernes",
                "Sabado"};

        String diaHoy = strDays[now.get(Calendar.DAY_OF_WEEK) - 1];
        for (int i = 0; i < list.size(); i++) {
            Schedules schedules = list.get(i);
            if (schedules.getDay_week().toUpperCase().equals(diaHoy.toUpperCase())) {

                String time = format.format(calendar.getTime()); //Formateo tiempo actual a 24 horas
                Calendar calTemp = Calendar.getInstance();
                calTemp.setTime(format.parse(schedules.getTime_start()));
                calTemp.add(Calendar.MINUTE, 5); //Agrego 5 min al tiempo del objeto que se esta recorriendo

                String timeObject = format.format(calTemp.getTime()); //Formateo a 24 horas el objeto
                System.out.println("Hora actual: " + time + " °°°°°°°°° Hola horario: " + timeObject);
                String asd = " /// " + time + " /// " + schedules.time_start + " /// " + timeObject;
                Log.e("INFO", asd);
                System.out.println(asd);

                Calendar calTempMenos5 = Calendar.getInstance(); //Avisar con notificacion que tiene clase en 5 min
                calTempMenos5.setTime(format.parse(schedules.getTime_start()));
                calTempMenos5.add(Calendar.MINUTE, -5); //Agrego 5 min al tiempo del objeto que se esta recorriendo
                String timeEvent = format.format(calTempMenos5.getTime());
/*
                if (time.compareTo(format.format(calTempMenos5.getTime())) > 0) {
                    if (counter == 0) {
                        sendNotification(schedules);
                        counter++;
                    }
                    //Enviar notificacion, el usuario tiene clase en los proximos 5 min
                }
*/

                if (time.compareTo(timeObject) > 0) {
                    socket.emit("connection:location", json);
                    //Enviar registro al soccet ya pasaron 5 min para estar en la universidad
                }
            }
            // TODO: 3/10/2019 Luego comparar si el tiempo del evento + 5 min es igual al tiempo actual, enviar ubicacion al servidor
            // TODO: 3/10/2019 El servidor hara la peticion para saber el lugar , luego poner condicional para saber si esta en la U,  
            // TODO: 3/10/2019 Si el usuario esta en la U enviar un booleano para habilitar el boton de enviar tema 
            // TODO: 3/10/2019 Si el usuario no esta en la universidad hacer un registro en la base de datos, que indica que el docente no estaba a la hora del evento en la Universidad 
        }

    }

    public void sendNotification(Schedules schedules) {

        int notifyID = 1;
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        String name = "Docentes";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            makeNotificationChannel(CHANNEL_ID, name, importance);
        }

        NotificationCompat.Builder notificacion = new NotificationCompat.Builder(this, CHANNEL_ID);

        notificacion.setSmallIcon(R.drawable.common_google_signin_btn_icon_dark_normal);
        notificacion.setContentTitle(schedules.getSubject() + " | " + schedules.getTime_start());
        notificacion.setContentText("En 5 min tendrá clases en " + schedules.getClassroom());
        notificacion.setColor(Color.BLUE);
        notificacion.setPriority(NotificationCompat.PRIORITY_MAX);
        notificacion.setDefaults(Notification.DEFAULT_SOUND);

        Intent notificatioIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificatioIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificacion.setContentIntent(contentIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.notify(notifyID, notificacion.build());


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    void makeNotificationChannel(String id, String name, int importance) {
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
       // mChannel.setShowBadge(true);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert mNotificationManager != null;
        mNotificationManager.createNotificationChannel(mChannel);
    }

    private void locationSetup() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                String time = format.format(calendar.getTime());
                String json = "{\"time\":" + "\"" + time + "\"" + ", \"usuario\":" + "\"" + user.getUserId() + "\"" + ",\"Latitude\":" + "\"" + location.getLatitude() + "\"" + ", \"Longitude\":" + "\"" + location.getLongitude() + "\"" + "}";
                locationText.setText("Latitude " + location.getLatitude() + " Longitude " + location.getLongitude());
                try {
                    verificar(json);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);
                return;
            }
        } else {
            configure();
        }
        //    locationManager.requestLocationUpdates("gps", 1000 * 60 * 1, (float) 1.5,  locationListener);
        locationManager.requestLocationUpdates("gps", MILISEGUNDOS_TIEMPO_GPS, 0, locationListener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    configure();
                return;
        }
    }

    @SuppressLint("MissingPermission")
    private void configure() {
        try {
            //locationManager.requestLocationUpdates("gps",  1000 * 60 * 1, (float) 1.5, locationListener);
            locationManager.requestLocationUpdates("gps", MILISEGUNDOS_TIEMPO_GPS, 0, locationListener);

        } catch (Exception e) {
            Log.e("location test", "configure: ", e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        configure();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Horario");

        user = new UserDao().getUser(this);
        if (user != null) {
            actionBar.setTitle(user.getFullName());
        } else {
            new DatabaseHelper(this).resetTables();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            actionBar.setTitle("OBJETO NULO");
        }

    }

    private void initNavigationMenu() {
        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_close:
                        new DatabaseHelper(getApplicationContext()).resetTables();
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        finish();
                        break;
                    case R.id.nav_daily_records:
                        startActivity(new Intent(getApplicationContext(), Daily_recordsActivity.class));
                        break;
                    case R.id.nav_location:
                        showConfirmDialog();
                        break;
                }
                actionBar.setTitle(item.getTitle());
                drawer.closeDrawers();
                return true;
            }
        });

        // open drawer at start
        //drawer.openDrawer(GravityCompat.START);
    }

    private void initComponent() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        //set data and list adapter
        mAdapter = new Adapter(this, list);
        recyclerView.setAdapter(mAdapter);

        // on item list clicked
        mAdapter.setOnItemClickListener(new Adapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Schedules obj, int position) {
                startActivity(new Intent(getApplicationContext(), DatailActivity.class).putExtra("schedules", obj));
                Snackbar.make(parent_view, "Item " + obj.subject + " clicked", Snackbar.LENGTH_SHORT).show();
            }
        });

    }

    private void getData(final boolean flag) {
        list = new ArrayList<>(0);
        String url = getString(R.string.base_url) + getString(R.string.schedules_url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray array = jsonObject.getJSONArray("response");
                    if (jsonObject.getBoolean("flag")) {
                        new DatabaseHelper(getApplicationContext()).resetSchedules();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            list.add(new Schedules(object.getInt("id"), object.getString("day_week"),
                                    object.getString("time_start"), object.getString("time_end"),
                                    object.getString("classroom"), object.getInt("user_id"),
                                    object.getInt("subject_id"), object.getString("subject"),
                                    object.getString("description")));
                        }
                        initComponent();
                        if (flag) swipe_refresh.setRefreshing(false);

                    } else {
                        Toast.makeText(MainActivity.this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        if (flag) swipe_refresh.setRefreshing(false);
                    }

                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    if (flag) swipe_refresh.setRefreshing(false);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                if (flag) swipe_refresh.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("identification", String.valueOf(user.getUserId()));
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void showConfirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Use Google's location services ?");
        builder.setMessage("latitud: " + "" + " Longitud: " + "");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Snackbar.make(parent_view, "Agree clicked", Snackbar.LENGTH_SHORT).show();
            }
        });
        builder.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();

    }
}
