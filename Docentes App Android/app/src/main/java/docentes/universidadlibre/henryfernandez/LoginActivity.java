package docentes.universidadlibre.henryfernandez;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import docentes.universidadlibre.henryfernandez.DataBase.UserDao;
import docentes.universidadlibre.henryfernandez.Models.User;

public class LoginActivity extends AppCompatActivity {

    private RequestQueue queue;
    private TextInputEditText txtId, txtPass;
    private RelativeLayout btnSignIn;
    private LinearLayout parent;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        queue = Volley.newRequestQueue(this);
        txtId = (TextInputEditText) findViewById(R.id.txtId);
        txtPass = (TextInputEditText) findViewById(R.id.txtPass);
        btnSignIn = (RelativeLayout) findViewById(R.id.btnSigIn);
        parent = (LinearLayout) findViewById(R.id.content);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!txtId.getText().toString().trim().isEmpty() && !txtPass.getText().toString().trim().isEmpty()) {
                    logIn();
                } else {
                    Toast.makeText(LoginActivity.this, "Llene los campos.", Toast.LENGTH_LONG).show();
                }
            }
        });

        if (new UserDao().getUser(this) != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    private void logIn() {
        progressBar.setVisibility(View.VISIBLE);
        parent.setVisibility(View.GONE);
        String url = getString(R.string.base_url) + getString(R.string.signin_url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject objeto = jsonObject.getJSONObject("response");
                            User u = new User(objeto.getInt("id"), objeto.getString("fullname"), objeto.getInt("rol"));
                            insertData(u);
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(LoginActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                            parent.setVisibility(View.VISIBLE);
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                        parent.setVisibility(View.VISIBLE);
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("identification", txtId.getText().toString().trim());
                params.put("password", txtPass.getText().toString().trim());
                return params;
            }
        };
        queue.add(stringRequest);
    }

    private void insertData(User user) {
        Boolean result = new UserDao().crate(user, this);
        Log.d("DOCENTES", "insertData: " + result);
    }

}
