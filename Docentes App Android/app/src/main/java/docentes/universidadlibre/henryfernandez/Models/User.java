package docentes.universidadlibre.henryfernandez.Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "user")
public class User {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(index = true, canBeNull = false)
    public int userId;

    @DatabaseField
    public String fullName;

    @DatabaseField
    public int rol;

    public User() {
    }

    public User(int userId, String fullName, int rol) {
        this.userId = userId;
        this.fullName = fullName;
        this.rol = rol;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "User{" +
                ", userId=" + userId +
                ", fullName='" + fullName + '\'' +
                ", rol=" + rol +
                '}';
    }
}
