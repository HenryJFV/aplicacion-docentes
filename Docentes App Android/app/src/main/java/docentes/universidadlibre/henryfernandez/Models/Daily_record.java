package docentes.universidadlibre.henryfernandez.Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "daily_records")
public class Daily_record {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String created_at;

    @DatabaseField
    private String subject;

    @DatabaseField
    private String topic;


    public Daily_record() {
    }

    public Daily_record(String created_at, String subject, String topic) {
        this.created_at = created_at;
        this.subject = subject;
        this.topic = topic;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "Daily_record{" +
                "created_at='" + created_at + '\'' +
                ", subject='" + subject + '\'' +
                ", topic='" + topic + '\'' +
                '}';
    }
}
